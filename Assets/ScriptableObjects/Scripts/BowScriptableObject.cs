using UnityEngine;

namespace GraveyAction
{
    [CreateAssetMenu(fileName = "BowWeapon", menuName = "GraveyAction/Weapon/Bow", order = 1)]
    public class BowScriptableObject : WeaponScriptableObject
    {
        public GameObject bow;
        public GameObject prefabArrow;
        public GameObject prefabVisualArrow;
        public float force;
        public float initialLateralTorque;
        public float timeToDestroy;
    }
}