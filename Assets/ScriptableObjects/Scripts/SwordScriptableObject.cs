using UnityEngine;

namespace GraveyAction
{
    [CreateAssetMenu(fileName = "SwordWeapon", menuName = "GraveyAction/Weapon/Sword", order = 2)]
    public class SwordScriptableObject : WeaponScriptableObject
    {
        public GameObject prefabSword;
    }
}