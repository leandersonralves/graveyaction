using UnityEngine;

namespace GraveyAction
{
    [CreateAssetMenu(fileName = "GenericWeapon", menuName = "GraveyAction/Weapon/Generic", order = 0)]
    public class WeaponScriptableObject : ScriptableObject
    {
        public enum AttackType
        {
            Sword,
            Bow
        }

        public float damage;

        public GameObject particleHitCharacter;

        public GameObject particleHitOthers;
    }
}