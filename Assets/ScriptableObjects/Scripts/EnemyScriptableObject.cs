﻿using System.Collections.Generic;
using UnityEngine;

namespace GraveyAction
{
    [CreateAssetMenu(fileName = "Character/Enemy", menuName = "GraveyAction/Enemy", order = 0)]
    public class EnemyScriptableObject : ScriptableObject
    {
        public enum AttackType
        {
            Sword,
            Bow
        }

        public GameObject prefabCharacterModel;

        [Header("Attack/Deffense settings.")]

        public WeaponScriptableObject weapon;

        public float defense;

        [Header("Behaviour Settings")]
        public float sqrDistPlayerFollow = 25f;

        public float sqrMinDistFollowStop = 4f;

        [Header("Health Settings")]
        public float maxHealth;

        [Header("Feedback Settings")]
        public GameObject particleDeath;

        public GameObject particleSpawn;
    }
}