﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GraveyAction
{
    public class Sword : MonoBehaviour, IWeapon
    {
        public float Damage { get { return _rightHandswordData.damage; } }

        [SerializeField]
        private float _distanceDamagePlayer = 15f;

        public float DistanceDamagePlayer { get { return _distanceDamagePlayer; } }

        private SwordScriptableObject _rightHandswordData;

        private Transform _rightHand;

        private IHealth _health;

        private float _sqrDistPlayer = 0f;

        private Transform _cacheTransform;

        private Transform _instancePlayer;

        private bool _isReady = false;

        public void SetData(SwordScriptableObject swordData)
        {
            this._rightHandswordData = swordData;
            GameObject weapon = Instantiate(swordData.prefabSword, _rightHand);
        }

        private void Awake()
        {
            _cacheTransform = transform;
            Animator animator = GetComponent<Animator>();
            _rightHand = animator.GetBoneTransform(HumanBodyBones.RightHand);
            transform.SetParent(_rightHand, false);
        }

        IEnumerator Start()
        {
            yield return new WaitUntil(() => GetComponent<IHealth>() != null);
            _health = GetComponent<IHealth>();

            while (!_instancePlayer)
            {
                _instancePlayer = GameObject.FindGameObjectWithTag("Player").transform;
                yield return null;
            }

            _isReady = true;
        }

        private void Update()
        {
            if (!_isReady) return;
            _sqrDistPlayer = (_cacheTransform.position - _instancePlayer.position).sqrMagnitude;
        }

        public void Hit()
        {
            if (!_isReady) return;
            if (_sqrDistPlayer < DistanceDamagePlayer)
            {
                _instancePlayer.GetComponent<IHealth>().ApplyDamage(this);
                GameObject.Instantiate(_rightHandswordData.particleHitCharacter, _rightHand.position, Quaternion.identity);
            }
            else
            {
                GameObject.Instantiate(_rightHandswordData.particleHitOthers, _rightHand.position, Quaternion.identity);
            }
        }

    }
}