﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GraveyAction
{
    [RequireComponent(typeof(Rigidbody))]
    public class Arrow : MonoBehaviour, IWeapon
    {
        public float Damage { get { return _bowData.damage; } }

        public float DistanceDamagePlayer { get; }

        private Rigidbody _cacheRigidbody;

        private BowScriptableObject _bowData;

        public void SetData (BowScriptableObject enemyData)
        {
            this._bowData = enemyData;
            //Initial torque to simulating alignment in fall to ground.
            _cacheRigidbody.AddRelativeTorque(Vector3.right * _bowData.initialLateralTorque, ForceMode.Force);
        }

        private void Awake()
        {
            _cacheRigidbody = GetComponent<Rigidbody>();
            if (_bowData != null)
            {
                //Initial torque to simulating alignment in fall to ground.
                _cacheRigidbody.AddRelativeTorque(Vector3.right * _bowData.initialLateralTorque, ForceMode.Force);
            }
        }

        IEnumerator Start()
        {
            yield return new WaitForSeconds(_bowData.timeToDestroy);
            GameObject.Destroy(gameObject);
        }

        public void OnCollisionEnter(Collision other)
        {
            Debug.Log("Arrow hitted " + other.gameObject.name);
            var heath = other.gameObject.GetComponent<Health>();
            if (heath)
            {
                heath.ApplyDamage(this);
                if (_bowData.particleHitCharacter)
                {
                    GameObject.Instantiate(_bowData.particleHitCharacter, other.contacts[0].point, Quaternion.identity);
                }
            }
            else
            {
                if (_bowData.particleHitOthers)
                {
                    GameObject.Instantiate(_bowData.particleHitOthers, other.contacts[0].point, Quaternion.identity);
                }
            }

            transform.SetParent(other.transform);
            _cacheRigidbody.isKinematic = true;
        }
    }
}