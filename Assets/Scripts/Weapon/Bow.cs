﻿using UnityEngine;

namespace GraveyAction
{
    [RequireComponent(typeof(Animator))]
    public class Bow : MonoBehaviour
    {
        [SerializeField]
        private Animator _animatorArcher;

        private int _hashParamFireAnimator = 0;
        private int _hashStatePulling = 0;
        private int _hashStateHold = 0;

        [SerializeField]
        private GameObject _visualArrow;

        [SerializeField]
        private BowScriptableObject _bowData;

        [SerializeField]
        private Transform _arrowPointInstance;

        [SerializeField]
        private Transform _bowPointInstance;

        private Transform _cacheTransform;

        private GameObject _cacheGameObject;

        void Awake()
        {
            _cacheTransform = transform;
            _cacheGameObject = gameObject;
            _animatorArcher = GetComponent<Animator>();

            _bowPointInstance = _animatorArcher.GetBoneTransform(HumanBodyBones.LeftHand);
            if (_bowPointInstance != null)
            {
                _cacheTransform.SetParent(_bowPointInstance, false);
            }

            if (_arrowPointInstance == null)
            {
                _arrowPointInstance = _animatorArcher.GetBoneTransform(HumanBodyBones.RightHand);
            }

            _hashParamFireAnimator = Animator.StringToHash("IsFiring");
            _hashStatePulling = Animator.StringToHash("ArcherPull");
            _hashStateHold = Animator.StringToHash("ArcherPullHold");
        }

        public void ReleaseArrow()
        {
            _animatorArcher.SetBool(_hashParamFireAnimator, false);
            var state = _animatorArcher.GetCurrentAnimatorStateInfo(0);
            float forceArrow = 0f;
            if (state.shortNameHash == _hashStateHold) { forceArrow = _bowData.force; }
            else if (state.shortNameHash == _hashStatePulling) { forceArrow = state.normalizedTime * _bowData.force; }

            Shoot(forceArrow);
        }

        public void Shoot()
        {
            Shoot(_bowData.force);
        }

        private void Shoot(float forceArrow)
        {
            GameObject instanceArrow = GameObject.Instantiate(_bowData.prefabArrow, _arrowPointInstance.position, _arrowPointInstance.rotation);

            Arrow arrow = instanceArrow.AddComponent<Arrow>();
            arrow.SetData(_bowData);

            instanceArrow.layer = _cacheGameObject.layer;
            int childCount = instanceArrow.transform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                instanceArrow.transform.GetChild(i).gameObject.layer = _cacheGameObject.layer;
            }

            instanceArrow.GetComponent<Rigidbody>().AddForce(_cacheTransform.forward * forceArrow, ForceMode.VelocityChange);
            _visualArrow.gameObject.SetActive(false);
        }

        internal void SetData(BowScriptableObject bowData)
        {
            this._bowData = bowData;

            _visualArrow = Instantiate(bowData.prefabVisualArrow, _arrowPointInstance);
            _visualArrow.transform.localPosition = _visualArrow.transform.localEulerAngles = Vector3.zero;
            _visualArrow.gameObject.SetActive(false);

            GameObject bow = Instantiate(bowData.bow, _bowPointInstance);
            bow.transform.localPosition = _visualArrow.transform.localEulerAngles = Vector3.zero;
        }

        public void PullArrow()
        {
            _visualArrow.gameObject.SetActive(true);
            _animatorArcher.SetBool(_hashParamFireAnimator, true);
        }
    }
}