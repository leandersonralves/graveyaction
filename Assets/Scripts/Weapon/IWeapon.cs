﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GraveyAction
{
    public interface IWeapon
    {
        float DistanceDamagePlayer {get; }

        float Damage { get; }
    }
}