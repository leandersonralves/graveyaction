using System;
using UnityEngine;

namespace GraveyAction
{
    public class Health : MonoBehaviour, IHealth
    {
        private Animator animator;

        public float CurrentHealth { get; private set; }

        private Action onDying;

        public Action OnDying { get { return onDying; } set { onDying = value; } }

        [SerializeField]
        private EnemyScriptableObject characterData;

        public float MaxHealth { get { return characterData.maxHealth; } }

        public float Defense { get { return characterData.defense; } }

        public void SetHealthData (EnemyScriptableObject healthData)
        {
            characterData = healthData;
            CurrentHealth = characterData.maxHealth;
            if (characterData.particleSpawn)
            {
                GameObject spawnInstance = GameObject.Instantiate(characterData.particleSpawn, transform.position, Quaternion.Euler(-90f, 0f, 0f));
            }
        }

        private bool hasAnimator = false;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            hasAnimator = animator != null;
            if (characterData != null)
            {
                SetHealthData(characterData);
            }
        }

        public void ApplyDamage(IWeapon weapon)
        {
            if (CurrentHealth <= 0) return;

            CurrentHealth -= Mathf.Clamp(weapon.Damage - Defense, 0f, float.PositiveInfinity);
            if (CurrentHealth <= 0f)
            {
                if (OnDying != null) OnDying.Invoke();
                if (hasAnimator) animator.SetBool("OnDying", true);
                if (characterData.particleSpawn)
                {
                    GameObject.Instantiate(characterData.particleDeath, transform.position, Quaternion.Euler(-90f, 0f, 0f));
                }
            }
            else
            {
                if (hasAnimator) animator.SetTrigger("OnHitted");
            }
            Debug.Log(name + " Hitted Health: " + CurrentHealth);
        }
    }
}