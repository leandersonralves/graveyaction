using System;

namespace GraveyAction
{
    public interface IHealth
    {
        float MaxHealth { get; }

        float CurrentHealth { get; }

        Action OnDying { get; set; }

        void ApplyDamage (IWeapon weapon);
    }
}