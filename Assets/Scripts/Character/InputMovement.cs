﻿using System;
using UnityEngine;

namespace GraveyAction
{
    [RequireComponent(typeof(CharacterController))]
    public class InputMovement : MonoBehaviour
    {
        [Header("Translate Speeds in m/s")]

        [SerializeField]
        private float forwardSpeed;

        [SerializeField]
        private float lateralSpeed;

        private CharacterController characterController;

        private Transform cacheTransform;

        private float _horizontalAxis, _verticalAxis, _jumpButton;

        private IHealth health;

        [SerializeField]
        private float _jumpForce = 500f;

        private void Awake()
        {
            cacheTransform = transform;
            characterController = GetComponent<CharacterController>();
            health = GetComponent<IHealth>();
            health.OnDying += OnDeath;
        }

        private void OnDeath()
        {
            var indexScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
            UnityEngine.SceneManagement.SceneManager.LoadScene(indexScene);
        }

        void Update()
        {
            _horizontalAxis = Input.GetAxis("Horizontal");
            _verticalAxis = Input.GetAxis("Vertical");

            if (Input.GetButtonDown("Jump") && characterController.isGrounded)
            {
                _jumpButton = _jumpForce;
            }

            _jumpButton += Physics.gravity.y * Time.deltaTime;

            characterController.Move(
                cacheTransform.forward * _verticalAxis * forwardSpeed +
                cacheTransform.right * _horizontalAxis * lateralSpeed + 
                Vector3.up * _jumpButton
            );
        }
    }
}