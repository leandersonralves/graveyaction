﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityRandom = UnityEngine.Random;

namespace GraveyAction
{
    public class Enemy : MonoBehaviour
    {
        private EnemyScriptableObject _enemyData;

        private static Transform _instancePlayer;

        private Transform _cacheTransform;

        private Animator _animator;

        private float _sqrDistPlayer = float.PositiveInfinity;

        private NavMeshAgent _agent;

        private IWeapon _weapon;
        public IHealth Health { get; private set; }

        private bool _isReady = false;

        public void SetEnemyData (EnemyScriptableObject enemyData)
        {
            this._enemyData = enemyData;

            _cacheTransform = transform;
            if (!_instancePlayer)
            {
                _instancePlayer = GameObject.FindGameObjectWithTag("Player").transform;
            }

            _weapon = GetComponentInChildren<IWeapon>();
            Health = GetComponent<IHealth>();

            _animator = GetComponent<Animator>();
            _agent = GetComponent<NavMeshAgent>();

            Health.OnDying += OnDeath;
            _isReady = true;
        }

        private void OnDeath()
        {
            _agent.enabled = false;
            enabled = false;
            var collider = GetComponentsInChildren<Collider>();
            for (var i = 0; i < collider.Length; i++)
            {
                collider[i].enabled = false;
            }
        }

        void FixedUpdate()
        {
            if (!_isReady) return;

            _sqrDistPlayer = (_cacheTransform.position - _instancePlayer.position).sqrMagnitude;
            _animator.SetFloat("sqrDistPlayer", _sqrDistPlayer);
            _animator.SetFloat("defenseProbability", UnityRandom.value);

            if (_sqrDistPlayer < _enemyData.sqrDistPlayerFollow)
            {
                if(_sqrDistPlayer > _enemyData.sqrMinDistFollowStop)
                {
                    _agent.isStopped = false;
                    _agent.destination = _instancePlayer.position;
                    _agent.updateRotation = true;
                    var normVelocity = _agent.velocity.normalized;
                    _animator.SetFloat("forward", Vector3.Dot(_cacheTransform.forward, normVelocity));
                    _animator.SetFloat("turn", Vector3.Dot(_cacheTransform.right, normVelocity));
                    _animator.SetBool("isFollowing", true);
                }
                else
                {
                    _agent.isStopped = true;
                    _agent.updateRotation = false;
                    _cacheTransform.forward = (_instancePlayer.position - _cacheTransform.position);
                    _animator.SetBool("isFollowing", false);
                }
            }
            else if (!_agent.isStopped)
            {
                _agent.isStopped = true;
                _animator.SetBool("isFollowing", false);
            }
        }

        public void FootL ()
        {
            //Debug.Log("Step Left!");
        }

        public void FootR ()
        {
            //Debug.Log("Step Right!");
        }
    }
}