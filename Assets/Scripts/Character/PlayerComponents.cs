﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GraveyAction
{
    public class PlayerComponents : MonoBehaviour
    {
        public static IHealth Health { get; private set; }

        public static Bow WeaponSelected { get; private set; }

        private void Awake()
        {
            Health = GetComponentInChildren<IHealth>();
            if (Health == null)
            {
                throw new System.NullReferenceException ("Player must have a Component implementing IHealth Interface.");
            }

            WeaponSelected = GetComponentInChildren<Bow>();
            if (WeaponSelected == null)
            {
                throw new System.NullReferenceException ("Player must have a ArcherWeapon Component.");
            }
        }
    }
}