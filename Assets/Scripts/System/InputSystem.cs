﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GraveyAction
{
    public class InputSystem : MonoBehaviour
    {
        [SerializeField]
        private GameStateManager _gameState;

        void Update()
        {
            if (_gameState.CurrentState != GameStateManager.State.Gameplay) return;

            if (Input.GetButtonDown("Fire1"))
            {
                PlayerComponents.WeaponSelected.PullArrow();
            }
            if (Input.GetButtonUp("Fire1"))
            {
                PlayerComponents.WeaponSelected.ReleaseArrow();
            }
        }
    }
}