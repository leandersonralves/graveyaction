﻿using UnityEngine;

public class DestroyAfterLifeTime : MonoBehaviour
{
    [SerializeField]
    private float _delayDestroy = 5f;

    void Awake()
    {
        Destroy(gameObject, _delayDestroy);
    }
}
