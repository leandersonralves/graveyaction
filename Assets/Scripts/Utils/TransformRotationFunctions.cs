﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformRotationFunctions : MonoBehaviour
{
    private Transform cacheTranform;

    private void Awake()
    {
        cacheTranform = transform;
    }

    public void SetRotationX(float x)
    {
        var v = cacheTranform.eulerAngles;
        v.x = x;
        cacheTranform.eulerAngles = v;
    }

    public void SetRotationY(float y)
    {
        var v = cacheTranform.eulerAngles;
        v.y = y;
        cacheTranform.eulerAngles = v;
    }

    public void SetRotationZ(float z)
    {
        var v = cacheTranform.eulerAngles;
        v.z = z;
        cacheTranform.eulerAngles = v;
    }
}
