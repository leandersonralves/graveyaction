﻿using UnityEditor;
using UnityEngine;
using GraveyAction.Managers;

namespace GraveyAction.Editor
{
    [CustomEditor(typeof(EnemyManager))]
    public class EnemyManagerEditor : UnityEditor.Editor
    {
        private EnemyManager instance;

        private void OnSceneGUI()
        {
            if (Event.current.type == EventType.Repaint)
            {
                instance = this.target as EnemyManager;
                if (instance.SpanwingAreas != null)
                {
                    Color cacheColor = Handles.color;
                    Handles.color = Color.yellow;
                    for (int i = 0; i < instance.SpanwingAreas.Length; i ++)
                    {
                        Handles.DrawWireDisc(instance.SpanwingAreas[i].point, Vector3.up, instance.SpanwingAreas[i].radius);
                    }
                    Handles.color = cacheColor;
                }
            }
        }
    }
}