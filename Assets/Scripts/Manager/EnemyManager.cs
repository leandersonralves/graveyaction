﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GraveyAction.Managers
{
    /// <summary>
    /// Class that define Spawn Info Areas.
    /// </summary>
    [System.Serializable]
    public class CircularArea
    {
        public Vector3 point;

        public float radius;

        public int initialAmount;

        [Tooltip("If is equals to 0, will instantiate all in Start")]
        public float instantiationInterval;

        [HideInInspector]
        public float nextSpawnTime;

        [HideInInspector]
        public List<Enemy> instancesEnemies = new List<Enemy>();

        [SerializeField]
        public EnemyScriptableObject enemyData;
    }

    public class EnemyManager : MonoBehaviour
    {
        private List<Enemy> _instanceEnemies = new List<Enemy>();


        /// <summary>
        /// Spawn Areas from scene.
        /// </summary>
        [SerializeField]
        private CircularArea[] _spawningAreas;

        private const string EnemyLayer = "Enemy";

#if UNITY_EDITOR
        //Property to only expose spawn areas in Editor.
        public CircularArea[] SpanwingAreas { get { return _spawningAreas; } }
#endif

        private void Start()
        {
            SpawnEnemies();
        }

        private void SpawnEnemies()
        {
            if (_spawningAreas == null || _spawningAreas.Length < 1)
            {
                Debug.LogWarning("Enemy Managers not found spawing areas.");
            }
            else
            {
                for (int areaIndex = 0; areaIndex < _spawningAreas.Length; areaIndex++)
                {
                    CircularArea spawnArea = _spawningAreas[areaIndex];
                    if (Mathf.Approximately(spawnArea.instantiationInterval, 0f))
                    {
                        for (int enemyIndex = 0; enemyIndex < spawnArea.initialAmount; enemyIndex++)
                        {
                            Vector2 randomPosition = Random.insideUnitCircle * spawnArea.radius;
                            Vector3 position = new Vector3(randomPosition.x + spawnArea.point.x, 0f, randomPosition.y + spawnArea.point.z);
                            spawnArea.instancesEnemies.Add(InstantiateEnemy(spawnArea.enemyData, position));
                        }
                    }
                }
            }
        }

        private void FixedUpdate()
        {
            for (int areaIndex = 0; areaIndex < _spawningAreas.Length; areaIndex++)
            {
                CircularArea spawnArea = _spawningAreas[areaIndex];
                if (spawnArea.instancesEnemies.Count < 1)
                {
                    if (spawnArea.instantiationInterval >= spawnArea.nextSpawnTime)
                    {
                        spawnArea.nextSpawnTime = Time.time + spawnArea.instantiationInterval;
                        for (int enemyIndex = 0; enemyIndex < spawnArea.initialAmount; enemyIndex++)
                        {
                            Vector2 randomPosition = Random.insideUnitCircle * spawnArea.radius;
                            Vector3 position = new Vector3(randomPosition.x + spawnArea.point.x, 0f, randomPosition.y + spawnArea.point.z);
                            spawnArea.instancesEnemies.Add(InstantiateEnemy(spawnArea.enemyData, position));
                        }
                    }
                }
                else
                {
                    spawnArea.nextSpawnTime = Time.time + spawnArea.instantiationInterval;
                }
            }
        }

        private Enemy InstantiateEnemy(EnemyScriptableObject enemyData, Vector3 position)
        {
            GameObject instance = GameObject.Instantiate(enemyData.prefabCharacterModel, position, Quaternion.identity);
            instance.AddComponent<UnityEngine.AI.NavMeshAgent>();
            instance.layer = LayerMask.NameToLayer(EnemyLayer);

            //TODO: Check best pratice using collider (attached to prefab or instance in runtime)
            // float height = 0f, radius = 0f;
            // MeshRenderer[] meshsRenderers = instance.GetComponentsInChildren<MeshRenderer>();
            // for (int i = 0; i < meshsRenderers.Length; i++)
            // {
            //     if (meshsRenderers[i].bounds.extents.y > height)
            //     {
            //         height = Mathf.Max(meshsRenderers[i].bounds.extents.y);
            //     }
            //     if (meshsRenderers[i].bounds.extents.x > radius)
            //     {
            //         radius = Mathf.Max(meshsRenderers[i].bounds.extents.x);
            //     }
            // }
            // CapsuleCollider capsuleCollider = instance.AddComponent<CapsuleCollider>();
            // capsuleCollider.height = height;
            // capsuleCollider.radius = radius;

            Enemy enemyComponent = instance.AddComponent<Enemy>();
            Health health = instance.AddComponent<Health>();

            //TODO: find a better way to create a Weapon specialized without Reflection. IT's the worst mode to do that :/
            if (enemyData.weapon is BowScriptableObject)
            {
                Bow bow = instance.AddComponent<Bow>();
                bow.SetData(enemyData.weapon as BowScriptableObject);
            }
            else if (enemyData.weapon is SwordScriptableObject)
            {
                Sword sword = instance.AddComponent<Sword>();
                sword.SetData(enemyData.weapon as SwordScriptableObject);
            }

            enemyComponent.SetEnemyData(enemyData);
            health.SetHealthData(enemyData);
            health.OnDying += OnDeathEnemy;

            _instanceEnemies.Add(enemyComponent);

            return enemyComponent;
        }

        private void OnDeathEnemy ()
        {
            GameData.EnemiesDowned++;
            for (int areaIndex = 0; areaIndex < _spawningAreas.Length; areaIndex++)
            {
                _spawningAreas[areaIndex].instancesEnemies.RemoveAll(x => x.Health.CurrentHealth <= 0f);
            }
        }
    }
}