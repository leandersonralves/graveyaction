﻿using System;
using UnityEngine;

namespace GraveyAction
{
    public class GameStateManager : MonoBehaviour
    {
        public enum State
        {
            Loading,
            Gameplay,
            MainMenu,
            OptionsMenu,
            GameOver,
            CreditsMenu
        }

        public bool IsPaused { get; private set; }

        private Action<bool> OnPauseStateChanged;
        private Action<State> OnGameStateChanged;

        public State CurrentState { get; private set; }

        private float _cacheTimeScale;

        public void RegisterCallbackPause(Action<bool> callback, bool defferedCall = false)
        {
            if (defferedCall)
            {
                callback.Invoke(IsPaused);
            }
            OnPauseStateChanged += callback;
        }

        public void UnregisterCallbackPause(Action<bool> callback)
        {
            OnPauseStateChanged -= callback;
        }

        public void RegisterCallbackGameState(Action<State> callback, bool defferedCall = false)
        {
            if (defferedCall)
            {
                callback.Invoke(CurrentState);
            }
            OnGameStateChanged += callback;
        }

        public void UnregisterCallbackGameState(Action<State> callback)
        {
            OnGameStateChanged -= callback;
        }


        public void SetPause(bool isPaused)
        {
            if (isPaused == IsPaused) return;

            IsPaused = isPaused;
            if (OnPauseStateChanged != null) OnPauseStateChanged.Invoke(IsPaused);

            if (IsPaused)
            {
                _cacheTimeScale = Time.timeScale;
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = _cacheTimeScale;
            }
        }

        public void ChangeState(State newState)
        {
            if (CurrentState == newState) { return; }

            CurrentState = newState;
            if (OnGameStateChanged != null) OnGameStateChanged.Invoke(CurrentState);
        }
    }
}