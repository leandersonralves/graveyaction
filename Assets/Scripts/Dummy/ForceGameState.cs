﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceGameState : MonoBehaviour
{
    [SerializeField]
    private GraveyAction.GameStateManager.State newState;

    private GraveyAction.GameStateManager.State cacheState;

    private GraveyAction.GameStateManager _gameStateInstance;

    private void Awake() {
        _gameStateInstance = GetComponent<GraveyAction.GameStateManager>();
    }

    private void Update()
    {
        if (cacheState == newState) return;

        cacheState = newState;
        _gameStateInstance.ChangeState(newState);
    }
}
