﻿using UnityEngine;

public class ThirdCameraMovement : MonoBehaviour
{
    private Transform cacheTransform;

    #region PARAMETROS PARA ROTACAO E TRANSALACAO DA CAMERA
    [SerializeField]
    public Vector3 offset = new Vector3(0.7f, 2.2f, -2.5f);

    [SerializeField]
    public Vector3 angularMouseVelocity = new Vector3(0f, 20f, 0f);

    [SerializeField]
    private LayerMask layerBlockCamera;

    [SerializeField]
    public float translateVelocity = 5f;

    //[SerializeField]
    //private float offsetHeightLookAt = 2f;

    private Vector3 lastMousePosition;

    private Vector3 deltaMousePosition;

    private Vector3 mouseEuler;

    private Vector3 nextPosition;
    #endregion

    [SerializeField]
    private Transform initialTarget;
    private Transform target;

    [SerializeField]
    public float maxAngleX = 35f;

    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
        nextPosition = PositionInterseted(target.position, Quaternion.Euler(0f, target.eulerAngles.y, 0f) * offset);
    }

    private bool IsTargetMoving()
    {
        return target != null && target.hasChanged;
    }

    Quaternion cameraRotation = Quaternion.identity;

    private void Awake()
    {
        cacheTransform = transform;
        SetTarget(initialTarget);
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null) return;

        #region CONTROLE ROTACAO CAMERA
        if (Input.GetMouseButtonDown(1))
        {
            lastMousePosition = Input.mousePosition;
            mouseEuler = cacheTransform.eulerAngles;
        }

        if (Input.GetMouseButton(1))
        {
            deltaMousePosition = Input.mousePosition - lastMousePosition;
            //Invertendo a rotacao em X.
            deltaMousePosition.y *= -1f;
            lastMousePosition = Input.mousePosition;

            mouseEuler.x += deltaMousePosition.y * angularMouseVelocity.x * Time.deltaTime;
            mouseEuler.y += deltaMousePosition.x * angularMouseVelocity.y * Time.deltaTime;

            mouseEuler.x = ClampAngle(mouseEuler.x);

            cameraRotation = Quaternion.Euler(mouseEuler);
            nextPosition = PositionInterseted(target.position, cameraRotation * offset);
        }
        else if (IsTargetMoving())
        {
            cameraRotation = Quaternion.Euler(0f, target.eulerAngles.y, 0f);
            nextPosition = PositionInterseted(target.position, cameraRotation * offset);
        }
        #endregion

        cacheTransform.position = Vector3.Lerp(cacheTransform.position, nextPosition, translateVelocity * Time.deltaTime);
        cacheTransform.LookAt(target.position);
    }

    private float ClampAngle(float currentAngle)
    {
        if (currentAngle > 360f) currentAngle %= 360f;
        if (currentAngle > 180f) currentAngle = currentAngle - 360f;
        return currentAngle = Mathf.Clamp(currentAngle, -maxAngleX, maxAngleX);
    }

    private float GetEulerY() { return cacheTransform.eulerAngles.y; }

    private Vector3 PositionInterseted(Vector3 origin, Vector3 offset)
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(origin, offset, out hitInfo, offset.magnitude, layerBlockCamera))
        {
            return hitInfo.point;
        }
        else
        {
            return origin + offset;
        }
    }
}
