﻿using UnityEngine;
using UnityEngine.Events;

public class FollowTransform : MonoBehaviour
{
    [SerializeField]
    private Transform follow;

    [SerializeField]
    private float initialLerpSpeed = 10f;

    private Transform cacheTransform;

    private bool locked = false;

    private Vector3 initialPosition;

    private float lerpT = 0;

    [SerializeField]
    private UnityEvent onLocked;

    private void Awake()
    {
        cacheTransform = transform;
        SetFollow(follow);
    }

    public void SetFollow(Transform follow)
    {
        this.follow = follow;
        lerpT = 0;
        locked = false;
        initialPosition = transform.position;
    }

    private void Update()
    {
        if (!locked)
        {
            cacheTransform.position = Vector3.Lerp(initialPosition, follow.position, lerpT);
            lerpT += Time.deltaTime * initialLerpSpeed;
            if ((cacheTransform.position - follow.position).sqrMagnitude < 0.001f)
            {
                locked = true;

                //.Net4 can be used onLocked?.Invoke() to simplify line.
                if (onLocked != null) onLocked.Invoke();
            }
        }
        else { cacheTransform.position = follow.position; }
    }
}
