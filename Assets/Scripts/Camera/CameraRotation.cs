﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEventFloat : UnityEvent<float> {}

public class CameraRotation : MonoBehaviour
{
    private Transform cacheTransform;

    [SerializeField]
    private bool followMouseMovement = true;

    public bool FollowMouseMovement
    {
        set
        {
            followMouseMovement = value;
            Cursor.visible = !value;
        }
    }

    private Vector3 lastMousePosition = Vector3.zero;
    private Vector3 rotation = Vector3.zero;

    [Header("Limit Angles")]
    [SerializeField]
    private float maxLowerAngleX;

    [SerializeField]
    private float minUpperAngleX;

    [Header("Settings Speed Angles")]
    [SerializeField]
    private float verticalAngleSpeed;

    [SerializeField]
    private float horizontalAngleSpeed;

    [SerializeField]
    private UnityEventFloat OnChangeY;

    [SerializeField]
    private UnityEventFloat OnChangeX;

    private void Awake()
    {
        cacheTransform = transform;
        FollowMouseMovement = true;
    }

    private void OnApplicationQuit() {
        FollowMouseMovement = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (followMouseMovement)
        {
            rotation.x += Input.GetAxis("Mouse Y") * verticalAngleSpeed * Time.deltaTime * -1f; //-1 to invert rotation.
            rotation.y += Input.GetAxis("Mouse X") * horizontalAngleSpeed * Time.deltaTime;
            rotation.x = ClampAngle(rotation.x);

            if (OnChangeY != null && !Mathf.Approximately(rotation.y, cacheTransform.eulerAngles.y))
            {
                OnChangeY.Invoke(rotation.y);
            }

            if (OnChangeX != null && !Mathf.Approximately(rotation.x, cacheTransform.eulerAngles.x))
            {
                OnChangeX.Invoke(rotation.x);
            }

            cacheTransform.eulerAngles = rotation;
        }
    }

    private float ClampAngle(float currentAngle)
    {
        if (currentAngle > 360f) currentAngle %= 360f;
        if (currentAngle > 180f) currentAngle = currentAngle - 360f;
        return currentAngle = Mathf.Clamp(currentAngle, minUpperAngleX, maxLowerAngleX);
    }
}
