﻿using System.Collections.Generic;

namespace GraveyAction
{
    public static class GameData
    {
        public static int EnemiesDowned { get; set; }

        public static void Reset ()
        {
            EnemiesDowned = 0;
        }
    }
}