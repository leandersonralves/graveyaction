﻿using UnityEngine;
using UnityEngine.UI;
using GraveyAction;


public class UIGameplayController : MonoBehaviour
{
    [Header("Health UI")]
    [SerializeField]
    private Text textHealth;

    [SerializeField]
    private Scrollbar barHealth;

    [Header("Enemies UI")]
    [SerializeField]
    private Text textDownedEnemies;

    private void FixedUpdate()
    {
        textHealth.text = PlayerComponents.Health.CurrentHealth.ToString();
        barHealth.size = PlayerComponents.Health.CurrentHealth / PlayerComponents.Health.MaxHealth;
        textDownedEnemies.text = GameData.EnemiesDowned.ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
